﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SafetyState
{
    Undetected = 0,
    Suspicion,
    Detected,
    Wanted,
    OnTheRun
}

public class Player : MonoBehaviour
{
    //Variable's
    [SerializeField] float f_speed;
    [SerializeField] GameObject fire;
    //Keycode's
    [SerializeField] KeyCode kc_Left;
    [SerializeField] KeyCode kc_Right;
    [SerializeField] KeyCode kc_Up;
    [SerializeField] KeyCode kc_Down;
    //Non Serialize field variable's
    private SafetyState safetystate;
    private PickUp pickup;

    public static bool b_OilFuel;
    public static bool b_Lighter;
    public static bool b_InPlant;

	void Update ()
    {
        //Go left
        if (Input.GetKey(kc_Left))
        {
            float speed = f_speed * Time.deltaTime;
            transform.position = new Vector3(transform.position.x - speed, transform.position.y, transform.position.z);
        } 
        //Go right
        if (Input.GetKey(kc_Right))
        {
            float speed = f_speed * Time.deltaTime;
            transform.position = new Vector3(transform.position.x + speed, transform.position.y, transform.position.z);
        }
        //Go up
        if(Input.GetKey(kc_Up))
        {
            float speed = f_speed * Time.deltaTime;
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + speed);
        }
        //Go down
        if (Input.GetKey(kc_Down))
        {
            float speed = f_speed * Time.deltaTime;
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - speed);
        }

        if(b_InPlant && b_Lighter && b_OilFuel)
        {
            if (Input.GetMouseButton(0))
            {
                fire.SetActive(true);
            }
        }
    }
}
