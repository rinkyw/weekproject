﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    //Variable's
    [SerializeField] Transform PlayerLoc;
    
	void Update ()
    {
        transform.position = new Vector3(PlayerLoc.position.x, transform.position.y, PlayerLoc.position.z);
	}
}
