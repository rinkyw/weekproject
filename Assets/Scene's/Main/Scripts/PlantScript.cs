﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantScript : MonoBehaviour
{
    void OnTriggerEnter(Collider collider)
    {
        Player.b_InPlant = true;
    }
    void OnTriggerExit(Collider collider)
    {
        Player.b_InPlant = false;
    }
}
