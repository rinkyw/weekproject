﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PickUp
{
    Oil = 1,
    Lighter = 2,
}

public class ObjectPickup : MonoBehaviour
{
    public PickUp pickup;

    void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Player")
        {
            PickUpSwitch();
            Destroy(gameObject);
        }
    }

    private void PickUpSwitch()
    {
        switch (pickup)
        {
            case PickUp.Oil:
                Player.b_OilFuel = true;
                //Debug.Log("oil");
                break;
            case PickUp.Lighter:
                Player.b_Lighter = true;
                //Debug.Log("lighter");
                break;
        }
    }
}
