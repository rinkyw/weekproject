﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToPlace : MonoBehaviour
{
    [SerializeField] Transform Location;

	void Update ()
    {
        transform.position = Location.position;
	}
}
