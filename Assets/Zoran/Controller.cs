﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public event System.Action OnReachedEndOfLevel;

    public float moveSpeed = 6;

    Rigidbody rigidb;
    Camera viewCamera;
    Vector3 Velocity;
    
	void Start ()
    {
        rigidb = GetComponent<Rigidbody>();
        viewCamera = Camera.main;
	}
	
	void Update ()
    {
        Vector3 mousePos = viewCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, viewCamera.transform.position.y));
        transform.LookAt(mousePos + Vector3.up * transform.position.y);
        Velocity = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized * moveSpeed; 
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Finish")
        {
            if(OnReachedEndOfLevel != null)
            {
                OnReachedEndOfLevel();
            }
        }
    }

    private void FixedUpdate()
    {
        rigidb.MovePosition(rigidb.position + Velocity * Time.fixedDeltaTime);
    }
}
