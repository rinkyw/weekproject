﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameUI : MonoBehaviour
{
    public GameObject gameLoseUI;
    public GameObject GameWinUI;
    bool gameIsWon;

    void Start()
    {
        Guard.OnGuardHasSpottedPLayer += ShowGameLoseUI;
        FindObjectOfType<Controller>().OnReachedEndOfLevel += ShowGameWinUI;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Application.Quit();
        }
        if (gameIsWon)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                SceneManager.LoadScene(1);
                gameIsWon = false;
                GameWinUI.SetActive(false);
            }
        }
    }

    void ShowGameWinUI()
    {
        GameWinUI.SetActive(true);
        gameIsWon = true;
        Guard.OnGuardHasSpottedPLayer -= ShowGameLoseUI;
    }

    void ShowGameLoseUI()
    {
        gameLoseUI.SetActive(true);
        Guard.OnGuardHasSpottedPLayer -= ShowGameWinUI;
        FindObjectOfType<Controller>().OnReachedEndOfLevel -= ShowGameWinUI;
    }

    private void OnDestroy()
    {
        Guard.OnGuardHasSpottedPLayer -= ShowGameLoseUI;
        Guard.OnGuardHasSpottedPLayer -= ShowGameWinUI;
    }
}
